import React, { useState, useEffect } from 'react'
import Button from 'react-bootstrap/Button'
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CModalBody,
  CModalContent,
  CTable,
  CTableBody,
  CModalFooter,
  CTableDataCell,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
  CHeaderBrand,
  CButton,
  CModal,
  CAlert,
} from '@coreui/react'
import API from './api/api'
import { Link } from 'react-router-dom'

export const Api = () => {
  const [data, setData] = useState([])
  const [loading, setLoading] = useState(true)
  const [visible, setVisible] = useState(false)

  useEffect(() => {
    getData()
  }, [])

  const getData = async () => {
    try {
      let response = await API.get('/')
      setData(response.data)
      setLoading(false)
    } catch (e) {
      console.log(e.message)
    }
  }

  const deleteData = (id) => {
    try {
      API.delete(`/${id}/`).then((res) => getData())
    } catch (e) {
      console.log(e.message)
    }
  }

  if (visible) {
    return (
      <>
        <h1>mantull</h1>
      </>
    )
  }

  return (
    <>
    <h1>{visible}</h1>
      <CCard className="bordered">
        <CCardHeader className="d-flex justify-content-between">
          <CHeaderBrand>
            <strong>Daftar</strong> <small>Name</small>
          </CHeaderBrand>
          <div className="d-grid gap-2 d-md-flex justify-content-md-end">
            <CButton color="info">
              <Link to={'/add'}>Tambah</Link>
            </CButton>
            <CButton className="float-right btn-sm" color="warning">
              Kembali
            </CButton>
          </div>
        </CCardHeader>

        <CCardBody>
          <CTable striped hover bordered borderColor="dark">
            <CTableHead color="dark">
              <CTableRow className="text-center">
                <CTableHeaderCell scope="col">ID</CTableHeaderCell>
                <CTableHeaderCell scope="col">Name</CTableHeaderCell>
                <CTableHeaderCell scope="col">Description</CTableHeaderCell>
                <CTableHeaderCell scope="col">Action</CTableHeaderCell>
              </CTableRow>
            </CTableHead>
            <CTableBody>
              {data.map((data, index) => {
                return (
                  <CTableRow className="text-center" key={index}>
                    <CTableDataCell>{data.id}</CTableDataCell>
                    <CTableDataCell>{data.title}</CTableDataCell>
                    <CTableDataCell>{data.description}</CTableDataCell>
                    <CTableDataCell>
                      <ButtonGroup size="sm">
                        <Button variant="danger" onClick={() => deleteData(data.id)}>
                          Delete
                        </Button>
                        <Button onClick={() => setVisible(false)}>Edit</Button>
                      </ButtonGroup>
                    </CTableDataCell>
                  </CTableRow>
                )
              })}
            </CTableBody>
          </CTable>
        </CCardBody>
      </CCard>
    </>
  )
}
export default Api
